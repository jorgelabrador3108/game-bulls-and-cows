<?php

use App\Http\Controllers\GameController;
use App\Models\Game;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/game', [GameController::class, 'index']);

Route::post('/game', [GameController::class, 'create']);

Route::post('/game/combination', [GameController::class, 'newCombination']);

// Route::put('/game/[id]', [GameController::class, 'update']);

Route::delete('/game/{id}', [GameController::class, 'delete']);

Route::get('/game/combination/{attempt}', [GameController::class, 'getPrevAttempt']);

