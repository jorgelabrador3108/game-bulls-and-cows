# game-bulls-and-cows

## Toros y Vacas

Toros y vacas es un juego tradicional inglés a lápiz y papel para dos jugadores cuyo objetivo es adivinar un número constituido por cuatro dígitos. 
En una hoja de papel, un jugador escribe un número de 4 dígitos y lo mantiene en secreto. Las cifras deben ser todas diferentes, no se debe repetir ninguna dentro del mismo número. El otro jugador trata de adivinar el número secreto en varios intentos que son anotados y numerados. 
En cada intento anota una cifra de cuatro dígitos completa, ésta es evaluada por el jugador que guarda el número secreto. Si una cifra está presente y se encuentra en el lugar correcto es evaluada como un toro, si una cifra está presente pero se encuentra en un lugar equivocado es evaluada como una vaca. La evaluación se anota al lado del intento y es pública. 
Al adivinarse el número termina la partida.

![Ejemplo de juego](/images/game.png)

## Repositorio para resolver este juego con Laravel 9.x, PHP 8.x y SQLite

## Notas y explicaciones sobre lógica entendida e implementada:

Los datos se guardan al crear una partida y con resto de datos en valores iniciales y solo se actualizan al finalizar una partida ya sea por ganar o perder, si no se termina se quedan los datos insertados y valores iniciales, en caso de perder y ganar se guarda la última combinación(similar al endpoint: combinación) Algunos valores no me parecen necesarios al perder como los puntos, ya que perdiendo podría ganarle a alguien que hubiera ganado, valga la redondancia. En una cache se van guardando endpoints de combinaciones intermediarios, tiempo de inicio de partida que se inicia al crear el juego, id de juego para no enviar por parámetros al enviar una combinación propuesta ya que la orden no especificaba que había que enviarlo.
Mi método de protección de información fue encriptar el nombre para guardarlo en base de datos

La tabla contiene: 
‘id’,
'name': nombre insertado cifrado ya que fue mi propuesta para proteger la información,
'age': edad insertada,
'combination': combinación a descubrir creada por una funcionalidad,
'evaluation': ultima evaluacion de juego, ejemplo: ‘2T 1C’ o ‘4T 0C’(ganado)
'cantAttempts': cantidad de intentos
'timeAvailable': tiempo restante
'points': puntuacion
'textStatus': estado: ganado, perdido, no finalizado

## Ejemplos de uso de los endpoints

-GET http://localhost:8000/api/game

-POST http://localhost:8000/api/game
content-type: application/json

{
    "name": "Jorge",
    "age": 28

}

-POST http://localhost:8000/api/game/combination
content-type: application/json

{
    "combination": 4576

}

-DELETE http://localhost:8000/api/game/1

-GET http://localhost:8000/api/game/combination/3



## Todos los puntos fueron resueltos
