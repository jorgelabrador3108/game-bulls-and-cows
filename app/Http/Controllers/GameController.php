<?php

namespace App\Http\Controllers;

use App\Models\Game;
use DateTime;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Str;
use Mockery\Undefined;

/**
* @OA\Info(
*             title="API Game bulls and cows",
*             version="1.0",
*             description="Game bulls and cows"
* )
*
* @OA\Server(url="http://localhost:8000")
*/

class GameController extends Controller
{

    private $combination;

    private $history;

    private $initialTime;

    private $time;

    private $idgame;

    function initCache($update = false) //init and update
    {
        $this->combination = $update ? $this->combination : $this->createCombination();
        $this->history = $update ? $this->history : array();
        $this->initialTime = $update ? $this->initialTime : strtotime(now());
        $this->time = env('GAME_TIME_IN_SECONDS');

        $this->idgame = $update ? $this->idgame : null;

        $cacheObject = array(
            'combination' => $this->combination,
            'history' => $this->history,
            'initialTime' => $this->initialTime,
            'time' => $this->time,
            'idgame' => $this->idgame
        );

        //cada que modifique aumenta el tiempo(no parece problema ahora)
        Cache::put('cacheObject', $cacheObject, $this->time + 300); //validar que haya pasado mucho tiempo

    }

    function getCache()
    {
        $cacheObject = Cache::get('cacheObject');

        if ($cacheObject) {
            $this->combination = $cacheObject['combination'];
            $this->history = $cacheObject['history'];
            $this->initialTime = $cacheObject['initialTime'];
            $this->time = $cacheObject['time'];
            $this->idgame = $cacheObject['idgame'];

            return $cacheObject;
        } else {
            return false;
        }

    }

    //cuando se inserta en base de datos por ganar o perder resetear cache
    function resetCache()
    {
        Cache::forget('cacheObject');
    }

    function createCombination()
    {
        $digits = [];

        for ($i = 0; $i < 4; $i++) {
            $digit = rand(0, 9);

            while (in_array($digit, $digits)) {
                $digit = rand(0, 9);
            }

            $digits[] = $digit;
        }
        $number = implode('', $digits);

        return $number;
    }

    function isCombination($combinationProposed, $combination)
    {
        $countBulls = 0;
        $countCows = 0;

        //tratamiento por si usuario repite numeros en la combinacion propuesta
        $founds = array();

        if ($combinationProposed == $combination) {
            return array(4, 0);
        } else {
            foreach (str_split($combinationProposed) as $pos => $digit) {
                if ($digit == $combination[$pos]) {
                    if (!in_array($digit, $founds)) {
                        $countBulls++;
                        $founds[] = $digit;
                    } else {
                        $countBulls++;
                        $countCows--;
                    }
                } else {
                    if (in_array($digit, str_split($combination)) && !in_array($digit, $founds)) {
                        $countCows++;
                        $founds[] = $digit;
                    }
                }
            }
        }

        return array($countBulls, $countCows);
    }

    /**
     * Get games
     * @OA\Get (
     *     path="/api/game",
     *     tags={"Game"},
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 type="array",
     *                 property="rows",
     *                 @OA\Items(
     *                     type="object",
     *                     @OA\Property(
     *                         property="id",
     *                         type="number",
     *                         example="1"
     *                     ),
     *                     @OA\Property(
     *                         property="name",
     *                         type="string",
     *                         example="Adgad454ansbm989sda"
     *                     ),
     *                     @OA\Property(
     *                         property="age",
     *                         type="number",
     *                         example="25"
     *                     ),
     *                     @OA\Property(
     *                         property="combination",
     *                         type="string",
     *                         example="1234"
     *                     ),
     *                      @OA\Property(
     *                         property="evaluation",
     *                         type="string",
     *                         example="4B 0C"
     *                     ),
     *                      @OA\Property(
     *                         property="cantAttempts",
     *                         type="number",
     *                         example="10"
     *                     ),@OA\Property(
     *                         property="timeAvailable",
     *                         type="number",
     *                         example="80"
     *                     ),@OA\Property(
     *                         property="points",
     *                         type="number",
     *                         example="60.5"
     *                     ),@OA\Property(
     *                         property="textStatus",
     *                         type="string",
     *                         example="Game Winner"
     *                     ),
     *                     @OA\Property(
     *                         property="created_at",
     *                         type="string",
     *                         example="2023-02-23T00:09:16.000000Z"
     *                     ),
     *                     @OA\Property(
     *                         property="updated_at",
     *                         type="string",
     *                         example="2023-02-23T12:33:45.000000Z"
     *                     ),
     *                 )
     *             )
     *         )
     *     )
     * )
     */
    function index()
    {
        $result = Game::get();
        return response()->json($result, 200);
    }

    /**
     * Create game
     * @OA\Post (
     *     path="/api/game",
     *     tags={"Game"},
     *     @OA\RequestBody(
     *         description="User details",
     *         required=true,
     *         @OA\JsonContent(
     *             @OA\Property(property="name", type="string", example="Carlos"),
     *             @OA\Property(property="age", type="number", example="18"),
     *         ),
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Created",
     *         @OA\JsonContent(
     *              @OA\Property(property="id", type="number", example=4)
     *         )
     *     ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request",
     *          @OA\JsonContent(
     *              @OA\Property(property="error", type="string", example="Name and age parameters are required"),
     *          )
     *      )
     * )
     */
    function create(Request $request)
    {
        if(!$request->name || !$request->age){
            return response()->json(array(
                'error' => 'Name and age parameters are required.'
            ), 400);
        }
        $this->initCache();
        $newGame = new Game;
        $newGame->name = Crypt::encryptString($request->name);
        $newGame->age = $request->age;
        $newGame->combination = $this->combination;

        //default values
        $newGame->evaluation = 0;
        $newGame->cantAttempts = 0;
        $newGame->timeAvailable = 0;
        $newGame->points = 0;
        $newGame->textStatus = "unfinished";

        $newGame->save();

        $this->idgame = $newGame->id;

        $this->initCache(true); //para obtener id en cache

        return response()->json(array('id'=>$newGame->id), 201);
    }

    function castObject($object, $result)
    {
        $result->evaluation = $object['evaluation'];
        $result->cantAttempts = $object['cantAttempts'];
        $result->timeAvailable = $object['timeAvailable'];
        $result->points = $object['points'];
        $result->textStatus = $object['textStatus'];

        return $result;
    }

        /**
     * Proposed new combination
     * @OA\Post (
     *     path="/api/game/combination",
     *     tags={"Game"},
     *     @OA\RequestBody(
     *         description="Proposed new combination",
     *         required=true,
     *         @OA\JsonContent(
     *             @OA\Property(property="combination", type="string", example="1234"),
     *         ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *         @OA\JsonContent(
     *              @OA\Property(property="combination", type="string", example=1234),
     *              @OA\Property(property="evaluation", type="string", example="2B 1C"),
     *              @OA\Property(property="cantAttempts", type="number", example="8"),
     *              @OA\Property(property="timeAvailable", type="number", example="130"),
     *              @OA\Property(property="points", type="number", example="0"),
     *              @OA\Property(property="ranking", type="number", example="0"),
     *              @OA\Property(property="textStatus", type="string", example="Continue!")
     *         )
     *     ),
     *     @OA\Response(
     *          response=400,
     *          description="Bad Request",
     *          @OA\JsonContent(
     *              @OA\Property(property="error", type="string", example="Name and age parameters are required")
     *          )
     *      ),
     * )
     */
    function newCombination(Request $request)
    {
        if(!$request->combination){
            return response()->json(array(
                'error' => 'Combination parameter is required.'
            ), 400);
        }

        $combination = (string)$request->combination;

        if(strlen($combination) != 4){
            return response()->json(array(
                'error' => 'The combination must be equal to 4 digits.'
            ), 400);
        }
        $validationCache = $this->getCache();

        //sirve para cuando pasa mucho tiempo o para
        //cuando gana o pierde el juego
        //e intenta mandar una nueva combinacion
        if ($validationCache === false) {
            return response()->json(array(
                'textStatus' => 'A lot of time has passed. Start a new game!!!'
            ), 400);
        }

        $id = $this->idgame;
        $game = Game::find($id);

        $current = strtotime(now());

        $result = array();

        if ($current - $this->initialTime >= $this->time) {
            $result = array(
                'combination' => $combination,
                'evaluation' => 'Not rated',
                'cantAttempts' => count($this->history), //no cuento el actual por perder
                'timeAvailable' => 0,
                'points' => 0,
                'ranking' => 'Not rated',
                'textStatus' => 'Game Over'
            );

            $game = $this->castObject($result, $game);

            $game->save();

            $result = array(
                'textStatus' => 'Game Over: The maximum game time has been reached',
                'combination' => $this->combination
            );
            $this->resetCache();
        } else {

            $found_key = array_search($combination, array_column($this->history, 'combination'));

            if ($found_key !== false) {
                return response()->json(array(
                    'textStatus' => 'Duplicate combination: The digits were previously sent in the same order.'
                ), 403);
            } else {

                $bullsAndCows = $this->isCombination($combination, $this->combination);

                if ($bullsAndCows[0] == 4) {

                    $points = ($current - $this->initialTime) / 2 + count($this->history) + 1;

                    $ranking = Game::where('points', '>', 0)
                        ->where('points', '<', $points)
                        ->count();

                    $result = array(
                        'combination' => $combination,
                        'evaluation' => $bullsAndCows[0] . 'B ' . $bullsAndCows[1] . 'C',
                        'cantAttempts' => count($this->history) + 1, //cuento el actual
                        'timeAvailable' => $this->time - ($current - $this->initialTime),
                        'points' => $points,
                        'ranking' => $ranking,
                        'textStatus' => 'Game Winner'
                    );

                    $game = $this->castObject($result, $game);
                    $game->save();
                    $this->resetCache();
                } else {

                    $result = array(
                        'combination' => $combination,
                        'evaluation' => $bullsAndCows[0] . 'B ' . $bullsAndCows[1] . 'C',
                        'cantAttempts' => count($this->history) + 1, //cuento el actual
                        'timeAvailable' => $this->time - ($current - $this->initialTime),
                        'points' => 0,
                        'ranking' => 0,
                        'textStatus' => 'Continue!'
                    );

                    $this->history[] = $result;
                    $this->initCache(true);
                }
            }
        }

        return response()->json($result, 200);
    }

/**
     * Delete game
     * @OA\Delete (
     *     path="/api/game/{id}",
     *     tags={"Game"},
     *     @OA\Parameter(
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *         @OA\JsonContent(
     *              @OA\Property(property="id", type="number", example=1),
     *              @OA\Property(property="name", type="string", example="eyJpdiI6Ik9TMDFEYnd6Unlv"),
     *              @OA\Property(property="age", type="number", example="17"),
     *              @OA\Property(property="combination", type="string", example="7168"),
     *              @OA\Property(property="evaluation", type="number", example="0"),
     *              @OA\Property(property="cantAttempts", type="number", example="0"),
     *              @OA\Property(property="timeAvailable", type="number", example="150"),
     *              @OA\Property(property="points", type="number", example="0"),
     *              @OA\Property(property="textStatus", type="string", example="Continue!"),
     *              @OA\Property(property="created_at", type="string", example="2023-02-23T12:33:45.000000Z"),
     *              @OA\Property(property="updated_at", type="string", example="2023-02-23T12:33:45.000000Z")
     *         )
     *     ),
     *      @OA\Response(
     *          response=404,
     *          description="NOT FOUND",
     *          @OA\JsonContent(
     *              @OA\Property(property="error", type="string", example="There is no game with that #id"),
     *          )
     *      )
     * )
     */
    function delete($id)
    {
        $gameO = Game::find($id);
        if ($gameO) {
            $game = Game::where('id', $id)->delete();
            return response()->json($gameO, 200);
        }

        return response()->json(['error' => "There is no game with that id"], 404);
    }

    /**
     * Show previous attempt
     * @OA\Get (
     *     path="/api/game/combination/{attempt}",
     *     tags={"Game"},
     *     @OA\Parameter(
     *         in="path",
     *         name="attempt",
     *         required=true,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *         @OA\JsonContent(
     *              @OA\Property(property="combination", type="string", example=1234),
     *              @OA\Property(property="evaluation", type="string", example="2B 1C"),
     *              @OA\Property(property="cantAttempts", type="number", example="8"),
     *              @OA\Property(property="timeAvailable", type="number", example="130"),
     *              @OA\Property(property="points", type="number", example="0"),
     *              @OA\Property(property="ranking", type="number", example="0"),
     *              @OA\Property(property="textStatus", type="string", example="Continue!")
     *         )
     *     ),
     *      @OA\Response(
     *          response=404,
     *          description="NOT FOUND",
     *          @OA\JsonContent(
     *              @OA\Property(property="error", type="string", example="Hasn't reached #attempt attempts yet"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description="NOT FOUND",
     *          @OA\JsonContent(
     *              @OA\Property(property="textStatus", type="string", example="A lot of time has passed. Start a new game!!!"),
     *          )
     *      )
     * )
     */
    function getPrevAttempt($attempt)
    {
        $validationCache = $this->getCache();

        if ($validationCache === false) {
            return response()->json(array(
                'textStatus' => 'A lot of time has passed. Start a new game!!!'
            ), 400);
        }

        if ($attempt > count($this->history)) {
            return response()->json(['error' => "Hasn't reached $attempt attempts yet"], 404);
        }

        return response()->json($this->history[$attempt - 2], 200);
    }
}
