<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class GameTest extends TestCase
{
    use RefreshDatabase;

    public function test_create_game()
    {
        $data = ['name' => 'Catalina', 'age' => 20];
        $response = $this->post('/api/game', $data);

        $response
            ->assertStatus(201);
            // ->assertDatabaseHas('games', $data);
    }

    public function test_send_combination()
    {
        $data = ['combination' => '20'];
        $response = $this->post('/api/game/combination', $data);

        $response
            ->assertStatus(400)
            ->assertJson(['textStatus' => "The combination must be equal to 4 digits."]);
    }

    public function test_delete_game()
    {
        $response = $this->delete('/api/game/999');

        $response
            ->assertStatus(404)
            ->assertJson(['error' => "There is no game with that id"]);
    }
}
